export function patchObject<A>(patchable: A, dto: Object): A {
  const copy = structuredClone(patchable)
  for (const [key, value] of Object.entries(dto)) {
    if (value != undefined && value != null && (patchable as any)[key] != value) {
      (copy as any)[key] = value
    }
  }
  return copy
}