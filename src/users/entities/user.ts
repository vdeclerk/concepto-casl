import { UserRole } from './roles';

export class User {
  userId: number;
  name: string;
  username: string;
  password: string;
  role: UserRole;
  department: string;
}
