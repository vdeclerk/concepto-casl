export enum UserRole {
  Seller  = 'seller',
  Painter = 'painter'
}
