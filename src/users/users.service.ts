import { Injectable } from '@nestjs/common';
import { User } from './entities/user';
import { UserRole } from './entities/roles';

@Injectable()
export class UsersService {
  private readonly users = [
    {
      userId: 1,
      name: 'Alice',
      username: 'alice',
      password: 'alice',
      role: UserRole.Seller,
      department: 'A',
    },
    {
      userId: 2,
      name: 'Bob',
      username: 'bob',
      password: 'bob',
      role: UserRole.Seller,
      department: 'B',
    },
    {
      userId: 3,
      name: 'Charly',
      username: 'charly',
      password: 'charly',
      role: UserRole.Painter,
      department: 'C',
    },
    {
      userId: 4,
      name: 'Daisy',
      username: 'daisy',
      password: 'daisy',
      role: UserRole.Painter,
      department: 'D',
    },
  ];

  async findByUsername(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.username === username);
  }

  async findOne(id: number): Promise<User | undefined> {
    return this.users[id];
  }
}
