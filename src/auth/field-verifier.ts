import { UserRole } from "../users/entities/roles";

type EnumDictionary<T extends string | symbol | number, U> = {
  [K in T]: U
}

export const allowedProductsFields: EnumDictionary<UserRole, string[]> = {
  [UserRole.Seller]: ['name', 'price'],
  [UserRole.Painter]: ['color']
}

export function getDtoFields(dto: any): string[] {
  return Object.entries(dto).map((entry) => { const [key, ] = entry; return key});
  // return dtoFields.filter((field) => allowedFields[user.role].indexOf(field) > -1).length == 0;
}