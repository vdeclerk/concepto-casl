import {
  InferSubjects,
  AbilityBuilder,
  AbilityClass,
  ExtractSubjectType,
  FieldMatcher,
  Ability
}                        from '@casl/ability';
import { Injectable }    from '@nestjs/common';
import { Action }        from '../entities/action.entity';
import { allowedProductsFields } from '../../auth/field-verifier';
import { Product }       from '../../product/entities/product.entity';
import { User }          from '../../users/entities/user';

type Subjects = InferSubjects<typeof Product | typeof User> | 'all';

export type AppAbility = Ability<[Action, Subjects]>;

const fieldMatcher: FieldMatcher = fields => field => fields.includes(field);

@Injectable()
export class CaslAbilityFactory {

  createForUser(user: User) {
    const { can, cannot, build } = new AbilityBuilder<Ability<[Action, Subjects]>>(
      Ability as AbilityClass<AppAbility>,
    );

    can(Action.Read, 'all', ['**']);
    
    can(Action.Update, Product, allowedProductsFields[user.role])
    cannot(Action.Update, Product, { category: {$ne: user.department }})
        .because("You can't update other department's products")

    return build({
      detectSubjectType: (item) =>
        item.constructor as ExtractSubjectType<Subjects>,
      fieldMatcher
    });

  }

}
