import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  private logger = new Logger(AuthService.name);

  async validateUser(username: string, pass: string): Promise<any> {
    this.logger.log(username);
    const user = await this.usersService.findByUsername(username);
    this.logger.log(user.userId);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      this.logger.log(result.name);
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
