import { Module } from '@nestjs/common';
import { UserRole } from './entities/roles';
import { UsersService } from './users.service';

@Module({
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
