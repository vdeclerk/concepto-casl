import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import {
  AppAbility,
  CaslAbilityFactory,
} from '../casl/casl-ability.factory/casl-ability.factory';
import { User } from '../users/entities/user';
import { CHECK_POLICIES_KEY } from './policy-decorator';
import { PolicyHandler } from './policy-handler';

@Injectable()
export class PoliciesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private caslAbilityFactory: CaslAbilityFactory,
  ) {}

  private logger = new Logger(PoliciesGuard.name);

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const policyHandlers =
      this.reflector.get<PolicyHandler[]>(
        CHECK_POLICIES_KEY,
        context.getHandler(),
      ) || [];

    const request = context.switchToHttp().getRequest();
    const { originalUrl, method, params, query, body, user } = request;

    this.logger.debug({ originalUrl, method, params, query, body, user });

    const ability = this.caslAbilityFactory.createForUser(user);

    return policyHandlers.every((handler) =>
      this.execPolicyHandler(handler, ability, user, body),
    );
  }

  private execPolicyHandler(handler: PolicyHandler, ability: AppAbility, user: User, body: Body) {
    if (typeof handler === 'function') {
      return handler(ability);
    }
    return handler.handle(ability, user, body);
  }
}
