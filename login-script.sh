TOKEN=$(curl -s -X POST http://localhost:3000/auth/login -d '{"username": "alice", "password": "alice"}' -H "Content-Type: application/json" | jq -r '.access_token')
#echo $TOKEN
echo "Alice - Seller"
curl -s -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1
echo
echo Update price
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"price": 1500}' -H "Content-Type: application/json"
echo
echo Update color
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"color": "Red"}' -H "Content-Type: application/json"
echo
sleep 1

echo
echo "Charly Painter"
TOKEN=$(curl -s -X POST http://localhost:3000/auth/login -d '{"username": "charly", "password": "charly"}' -H "Content-Type: application/json" | jq -r '.access_token')
#echo $TOKEN
curl -s -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/3
echo
echo Update price
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/3 -d '{"price": 1500}' -H "Content-Type: application/json"
echo
echo Update color
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/3 -d '{"color": "Red"}' -H "Content-Type: application/json"
echo
sleep 1

echo
echo "Charly Painter on wrong department"
TOKEN=$(curl -s -X POST http://localhost:3000/auth/login -d '{"username": "charly", "password": "charly"}' -H "Content-Type: application/json" | jq -r '.access_token')
#echo $TOKEN
curl -s -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1
echo
echo Update price
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"price": 1500}' -H "Content-Type: application/json"
echo
echo Update color
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"color": "Red"}' -H "Content-Type: application/json"
echo
sleep 1

echo
echo "Bob Seller on wrong department"
TOKEN=$(curl -s -X POST http://localhost:3000/auth/login -d '{"username": "bob", "password": "bob"}' -H "Content-Type: application/json" | jq -r '.access_token')
#echo $TOKEN
curl -s -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1
echo
echo Update price
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"price": 1500}' -H "Content-Type: application/json"
echo
echo Update color
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"color": "Red"}' -H "Content-Type: application/json"
echo
sleep 1

echo
echo "Daisy Painter on wrong department"
TOKEN=$(curl -s -X POST http://localhost:3000/auth/login -d '{"username": "daisy", "password": "daisy"}' -H "Content-Type: application/json" | jq -r '.access_token')
#echo $TOKEN
curl -s -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1
echo
echo Update price
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"price": 1500}' -H "Content-Type: application/json"
echo
echo Update color
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"color": "Red"}' -H "Content-Type: application/json"
echo
sleep 1

echo
echo "Alice wrong password"
TOKEN=$(curl -s -X POST http://localhost:3000/auth/login -d '{"username": "alice", "password": "wrong"}' -H "Content-Type: application/json" | jq -r '.access_token')
#echo $TOKEN
curl -s -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1
echo
echo Update price
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"price": 1500}' -H "Content-Type: application/json"
echo
echo Update color
curl -s -X PATCH -H "Authorization: Bearer ${TOKEN}" http://localhost:3000/product/1 -d '{"color": "Red"}' -H "Content-Type: application/json"
echo
