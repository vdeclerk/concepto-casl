import { User } from 'src/users/entities/user';
import { AppAbility } from '../casl/casl-ability.factory/casl-ability.factory';

interface IPolicyHandler {
  handle(ability: AppAbility): boolean;
}

export interface IUserAndBodyAwarePolicyHandler {
  handle(ability: AppAbility, user: User, body: Body): boolean;
}

type PolicyHandlerCallback = (ability: AppAbility) => boolean;

export type PolicyHandler = IPolicyHandler | IUserAndBodyAwarePolicyHandler | PolicyHandlerCallback;
