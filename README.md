
## Description

This is an example application for testing CASL capabilities.
It has a hardcoded secretKey and four hardcoded users.
There are two roles: sellers can change the name and the price of the products, and painters can change the color of the products.
Users can modify only the products with a category equal to their department name.

The `login-script.sh` make requests to test the different scenarios.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```