export class Product {
  
  static get modelName() {
    return 'Product';
  }

  id: number;
  price: number;
  name: string;
  color: string;
  category: string;
}
