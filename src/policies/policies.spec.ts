import { Test, TestingModule } from '@nestjs/testing';
import { CaslAbilityFactory } from '../casl/casl-ability.factory/casl-ability.factory';
import { CaslModule } from '../casl/casl.module';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { UpdateProductPolicyHandler } from './update-product-policy';
import { UpdateProductDto } from '../product/dto/update-product.dto';

describe('Update product policy handler', () => {

  let handler: UpdateProductPolicyHandler
  let usersService: UsersService
  let abilityFactory: CaslAbilityFactory

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UsersModule, CaslModule],
      providers: [UsersService, CaslAbilityFactory],
    }).compile();
    usersService = module.get<UsersService>(UsersService);
    abilityFactory = module.get<CaslAbilityFactory>(CaslAbilityFactory)
  });

  beforeAll( () => {
    handler = new UpdateProductPolicyHandler();
  });

  it('should be defined', () => {
    expect(handler).toBeDefined();
    expect(usersService).toBeDefined();
  });

  it('shoud authorize Alice to modify price', () => {
    usersService.findByUsername('alice').then(alice => {
      const dto = new UpdateProductDto()
      dto.price = 1234
      expect(handler.handle(abilityFactory.createForUser(alice), alice, dto)).toBeTruthy
    })
  });

  it('shoud not authorize Alice to modify color', () => {
    usersService.findByUsername('alice').then(alice => {
      const dto = new UpdateProductDto()
      dto.color = "Red"
      expect(handler.handle(abilityFactory.createForUser(alice), alice, dto)).toBe(false)
    })
  });
});
