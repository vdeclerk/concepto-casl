import { JwtModule, JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UsersModule, JwtModule],
      providers: [AuthService, JwtService],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('shoud validate Alice', () => {
    service.validateUser('alice', 'alice').then((data) => {
      expect(data).toBeDefined();
      expect(data.username).toBe('alice');      
      expect(data.userId).toBe(1);
    });
  });

  it('should validate Charly', () => {
    service.validateUser('charly', 'charly').then((data) => {
      expect(data.userId).toBe(3);
    });
  });
});
