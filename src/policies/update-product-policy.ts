import { handleRetry } from "@nestjs/typeorm";
import { getDtoFields } from "../auth/field-verifier";
import { AppAbility } from "../casl/casl-ability.factory/casl-ability.factory";
import { Action } from "../casl/entities/action.entity";
import { Product } from "../product/entities/product.entity";
import { User } from "../users/entities/user";
import { IUserAndBodyAwarePolicyHandler } from "./policy-handler";
import { Logger } from '@nestjs/common';

export class UpdateProductPolicyHandler implements IUserAndBodyAwarePolicyHandler {

  private logger = new Logger(UpdateProductPolicyHandler.name)

  handle(ability: AppAbility, user: User, body: any): boolean {
    return getDtoFields(body).reduce<boolean>( (result, field) => 
        result && ability.can(Action.Update, Product, field), true)
  }
}

// return getDtoFields(body).reduce<boolean>( (result, field) => {
//   if (body[field] != undefined && body[field] != null) {
//     result && ability.can(Action.Update, Product, field)
//   } else {
//     result
//   }
// }, true)