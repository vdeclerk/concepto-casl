import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should find Alice', () => {
    service.findByUsername('alice').then((user) => {
      expect(user).toBeDefined();
      expect(user.name).toBe('Alice');
    });
  });

  it('should find Charly', () => {
    service.findByUsername('charly').then((user) => {
      expect(user).toBeDefined();
      expect(user.name).toBe('Charly');
    });
  });
});
