import { ForbiddenError } from '@casl/ability';
import { Injectable, Logger } from '@nestjs/common';
import { CaslAbilityFactory } from 'src/casl/casl-ability.factory/casl-ability.factory';
import { Action } from 'src/casl/entities/action.entity';
import { User } from 'src/users/entities/user';
import { patchObject } from 'src/utility-functions';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductService {
  constructor(private abilityFactory: CaslAbilityFactory) {}

  private logger = new Logger(ProductService.name)

  private products = [
    {
      id: 1,
      name: 'Puerta',
      price: 1400,
      color: 'Blanco',
      category: 'A',
    },
    {
      id: 2,
      name: 'Ventana',
      price: 900,
      color: 'Blanco',
      category: 'B',
    },
    {
      id: 3,
      name: 'Ventiluz',
      price: 400,
      color: 'Blanco',
      category: 'C',
    },
    {
      id: 4,
      name: 'Reja',
      price: 1800,
      color: 'Negro',
      category: 'E',
    },
  ];

  create(createProductDto: CreateProductDto) {
    return 'This action adds a new product';
  }

  findAll() {
    return this.products;
  }

  findOne(id: number) {
    const data = this.products.find((prod) => prod.id === id)
    const product = new Product()
    product.id = data.id
    product.category = data.category
    product.color = data. color
    product.name = data.name
    product.price = data.price
    return product
  }

  update(id: number, updateProductDto: UpdateProductDto, user: User) {
    const ability = this.abilityFactory.createForUser(user);
    const productToUpdate: Product = this.findOne(id);
    this.logger.debug(user)
    this.logger.debug(productToUpdate)
    this.logger.debug(ability.can(Action.Update, productToUpdate))
    ForbiddenError.from(ability).throwUnlessCan(Action.Update, productToUpdate)

    return `This action updates a #${productToUpdate.id} product`;
  }

  remove(id: number) {
    return `This action removes a #${id} product`;
  }
}
